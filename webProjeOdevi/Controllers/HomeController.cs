﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Web;
using System.Web.Mvc;
using webProjeOdevi.DAL;

namespace webProjeOdevi.Controllers
{
    public class HomeController : Controller
    {
        webProjeEntities db = new webProjeEntities();

        public ActionResult Index()
        {
            
            return View();
        }

        public ActionResult Admin()
        {

            return View();
        }

        public ActionResult ItemList(int CAT_ID)
        {
            var urunler = db.Items.Where(u => u.ITM_Category == CAT_ID);
            return View(urunler.ToList());
        }
        public ActionResult ItemListAll()
        {
            var urunler = db.Items;
            return View(urunler.ToList());
        }

        public ActionResult Item(int ITM_ID)
        {
            var urunler = db.Items.Where(u => u.ITM_ID == ITM_ID);
            return View(urunler.ToList());
        }
        [HttpPost]
        public ActionResult SepeteEkle(Item item, int ITM_ID)
        {
            try
            {
                //Item birim = new Item();
                Cart cart = new Cart();
                cart.CRT_Item = item.ITM_ID;
                cart.CRT_User = int.Parse(Session["userID"].ToString());
                cart.CRT_Date = DateTime.Now;
                db.Carts.Add(cart);
                db.SaveChanges();
                return RedirectToAction("ItemListAll");

            }
            catch (Exception ex)
            {
                throw new Exception("Sepete Eklemede Hata Oluştu", ex.InnerException);
            }
            
        }

        public ActionResult Cart()
        {
            var id = int.Parse(Session["userID"].ToString());
            var urunler = db.Carts.Where(u => u.CRT_User == id);
            return View(urunler.ToList());
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Login()
        {
            ViewBag.Message = "Your contact page.";
            Session["giris"] = null;
            return View();
        }
        public ActionResult Register()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GirisYardimci(GirisClass girisveriler)
        {
            var user = db.Users.Where(x => x.USR_Username == girisveriler.username && x.USR_Password == girisveriler.password).FirstOrDefault();
            
            if (user != null)
            {
                Session["username"] = user.USR_Username;
                Session["name"] = user.USR_Name;
                Session["surname"] = user.USR_Surname;
                Session["userID"] = user.USR_ID;
                Session["role"]=user.USR_Role;
                Session["giris"] = true;

                TempData["mesaj"] = "Hoşgeldiniz";

                if(Convert.ToInt32(Session["role"]) == 1)
                {
                    return RedirectToAction("Admin", "Home");
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
                
            }
            else
            {
                Session["giris"] = false;

            }
            ViewBag.Message = "Your contact page.";

            TempData["mesaj"] = "Giriş bilgileriniz yanlış";
            return RedirectToAction("Giris", "Home");

        }

        public ActionResult Kaydol(User kaydolveriler)
        {
            try
            {
                User _kaydolveriler = new User();
                _kaydolveriler.USR_Name = kaydolveriler.USR_Name;
                _kaydolveriler.USR_Surname = kaydolveriler.USR_Surname;
           
                _kaydolveriler.USR_Username = kaydolveriler.USR_Username;
                _kaydolveriler.USR_Password = kaydolveriler.USR_Password;
                _kaydolveriler.USR_Email = kaydolveriler.USR_Email;
                _kaydolveriler.USR_Role = 2;
                db.Users.Add(_kaydolveriler);
                db.SaveChanges();
                ViewBag.Message = "Kullanici Kaydı Başarıyla Tamamlanmıştır";

                Session["username"] = kaydolveriler.USR_Username;
                Session["name"] = kaydolveriler.USR_Name;
                Session["surname"] = kaydolveriler.USR_Surname;
                Session["userID"] = kaydolveriler.USR_ID;
                Session["role"] = kaydolveriler.USR_Role;
                Session["giris"] = true;


                return RedirectToAction("Index");

            }
            catch (Exception ex)
            {
                throw new Exception("Eklerken hata oluştu", ex.InnerException);
            }

        }

        public class GirisClass
        {
            public string username { get; set; }
            public string password { get; set; }

        }
    }
}